import * as $ from "jquery";
import jQuery from "jquery";
import _helpers from "../scss/_helpers.scss";
import _layout from "../scss/_layout.scss";
import _reset from "../scss/_reset.scss";
import _variables from "../scss/_variables.scss";
import styles from "../scss/styles.scss";
import _tabs from "../scss/modules/_tabs.scss";

(function ($) {
    "use strict";

    const container = document.querySelector(".img-content-m");
    const image = document.querySelector(".magnifier").style;

    container.addEventListener("mousemove", (e) => {
        let x = e.offsetX;
        let y = e.offsetY;

        let width = e.target.offsetWidth;
        let height = e.target.offsetHeight;

        let xPercentage = x / (width / 2) - 1;
        let yPercentage = y / (height / 2) - 1;
        image.transform = `translate3d(${xPercentage * -16}px, ${
            yPercentage * -16
        }px, 0px) scale(1.1)`;
    });

    container.addEventListener("mouseleave", () => {
        image.transform = `translate3d(0px,0px,0px)`;
    });

    var drag = false;
    $("#src")
        .on("mousedown", function (e) {
            drag = true;
        })
        .on("mouseup mouseout", function () {
            $(this).data({
                startX: 0,
                startY: 0,
            });
            drag = false;
        })
        .on("mousemove", function (e) {
            e.preventDefault();
            if (drag) {
                console.log($(this).data(), e);
                var left = parseInt($(this).css("left")) || 0,
                    top = parseInt($(this).css("top")) || 0,
                    newLeft =
                        left +
                        (e.clientX - ($(this).data().startX || e.clientX)),
                    newTop =
                        top +
                        (e.clientY - ($(this).data().startY || e.clientY)),
                    parentHeight = $(this).parent().height(),
                    parentWidth = $(this).parent().width(),
                    imgHeight = $(this).height(),
                    imgWidth = $(this).width();

                $(this)
                    .css({
                        left:
                            newLeft < 0 &&
                            Math.abs(newLeft - parentWidth) < imgWidth
                                ? newLeft
                                : left,
                        top:
                            newTop < 0 &&
                            Math.abs(newTop - parentHeight) < imgHeight
                                ? newTop
                                : top,
                    })
                    .data({
                        startX: e.clientX,
                        startY: e.clientY,
                    });
            }
        });

    function myFunc() {
        var x = document.getElementById("id");
        console.log("click");

        if (x.style.display === "none") {
            console.log("open");
            x.style.display = "block";
        } else {
            console.log("close");
            x.style.display = "none";
        }
    }

    document.querySelectorAll("button").forEach((item) => {
        item.addEventListener("click", myFunc);
    });

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self
            .find(".js-tab-header")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });
        var $tabContent = $self
            .find(".js-tab-content")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass("active").eq(index).addClass("active");
            $tabContent.removeClass("active").eq(index).addClass("active");
        };

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on("click", function () {
                selectTab($(this).index());
            });
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $(".js-tabs").each(function () {
        $(this).data("tabs", $(this).tabs());
    });
})(jQuery);
